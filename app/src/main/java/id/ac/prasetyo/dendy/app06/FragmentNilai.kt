package id.ac.prasetyo.dendy.app06

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_nilai.*
import kotlinx.android.synthetic.main.frag_data_nilai.view.*
import kotlinx.android.synthetic.main.frag_perkalian.view.*

class FragmentNilai : Fragment(), View.OnClickListener {

    val MhsSelect : AdapterView.OnItemSelectedListener = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            sp1.setSelection(0,true)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val c : Cursor = spAdapter2.getItem(position) as Cursor
            namaMhs = c.getString(c.getColumnIndex("_id"))
        }
    }
    val matkulselect : AdapterView.OnItemSelectedListener = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            sp2.setSelection(0,true)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val c : Cursor = spAdapter.getItem(position) as Cursor
            namaMk1 = c.getString(c.getColumnIndex("_id"))
        }
    }
    lateinit var thisParent : MainActivity
    lateinit var v : View
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter : SimpleCursorAdapter
    lateinit var spAdapter2 : SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    var namaMhs : String =""
    var namaMk1 : String =""
    lateinit var db : SQLiteDatabase



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v =  inflater.inflate(R.layout.frag_data_nilai,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDelleteNilai.setOnClickListener(this)
        v.btnInsertNilai.setOnClickListener(this)
        v.btnUpdataeNilai.setOnClickListener(this)
        v.sp1.onItemSelectedListener = MhsSelect
        v.sp2.onItemSelectedListener = matkulselect
        return v
    }

    override fun onClick(v: View?) {
        when (v?.id){
            R.id.btnDelleteNilai ->{

            }
            R.id.btnInsertNilai->{
                dialog.setTitle("KONFIRMASI").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah Data yang DiMasukan Sudah Benar ?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak,null")
                dialog.show()
            }
            R.id.btnUpdataeNilai ->{

            }
        }
    }

    override fun onStart() {
        super.onStart()

    }

    fun showData(){
        var sql = "select nilai.nomer as _id, mhs.nama, matkul.namamk, nilai.nilai from,mhs, matkul " +
                " where nilai.nim = mhs.nim and nilai.kodemk = matkul.kodemk"
        val c : Cursor = db.rawQuery(sql, null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.frag_data_nilai,c,
        arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.sp2.adapter = spAdapter
        v.sp2.setSelection(0)
    }
    fun showDataMatkul(){
        val c : Cursor = db.rawQuery("select kodemk, namamk as _id, id_prodi from matkul ",null)
        spAdapter2 = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_dropdown_item,c,
            arrayOf("_id"),
            intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.sp2.adapter = spAdapter2
        v.sp2.setSelection(0)
    }
    fun showDataMhs(){
        val cd : Cursor = db.rawQuery("select nim, nama as _id, id_prodi from mhs",null)
        spAdapter2 = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_dropdown_item,cd,
            arrayOf("_id"),
            intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.sp1.adapter = spAdapter2
        v.sp1.setSelection(0)
    }

    fun insertDataNilai(nim: String, kodemk : String, nilai : String){
        var sql = "insert into nilai( nim, kodemk , nilai) values (?,?,?)"
        db.execSQL(sql, arrayOf(nim,kodemk,nilai))
        showDataMatkul()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select nim from mhs where nama='$namaMhs'"
        var sql2 = "select kodemk from matkul where namamk='$namaMk1'"
        val c : Cursor = db.rawQuery(sql,null)
        val c1 : Cursor = db.rawQuery(sql2,null)
        if(c.count>0 && c1.count>0){
            c.moveToFirst()
            c1.moveToFirst()
            insertDataNilai(
                c.getString(c.getColumnIndex("nim")),
                c1.getString(c1.getColumnIndex("kodemk")),
                v.edNilai.text.toString()
            )
            v.edNilai.setText("")
        }
    }
}