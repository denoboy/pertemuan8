package id.ac.prasetyo.dendy.app06

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.jvm.internal.PropertyReference0

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var db : SQLiteDatabase
    lateinit var fragProdi : FragmentProdi
    lateinit var fragMhs : FragmentMahasiswa
    lateinit var ft : FragmentTransaction
    lateinit var fragBagi : FragmentBagi
    lateinit var fragKali : FragmentKali
    lateinit var fragnilai : FragmentNilai

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragProdi = FragmentProdi()
        fragMhs = FragmentMahasiswa()
        db = DBOpenHelper(this).writableDatabase
        fragBagi = FragmentBagi()
        fragKali = FragmentKali()
        fragnilai = FragmentNilai()

    }

    fun getDbObject() : SQLiteDatabase{
        return db
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemMatkul ->{
                Toast.makeText(this,"Playing Music", Toast.LENGTH_SHORT).show()
                return true
            }
            R.id.itemNilai ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.framelayout,fragnilai).commit()
                framelayout.setBackgroundColor(Color.argb(245,255,255,225))
                framelayout.visibility = View.VISIBLE
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itemProdi ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.framelayout,fragProdi).commit()
                framelayout.setBackgroundColor(Color.argb(245,255,255,225))
                framelayout.visibility = View.VISIBLE
            }
            R.id.itemMhs ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.framelayout,fragMhs).commit()
                framelayout.setBackgroundColor(Color.argb(245,255,225,255))
                framelayout.visibility = View.VISIBLE
            }
            R.id.itemPembagian ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.framelayout,fragBagi).commit()
                framelayout.setBackgroundColor(Color.argb(245,255,225,255))
                framelayout.visibility = View.VISIBLE
            }
            R.id.itemPerkalian ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.framelayout,fragKali).commit()
                framelayout.setBackgroundColor(Color.argb(245,255,225,255))
                framelayout.visibility = View.VISIBLE
            }
            R.id.itemAbout -> framelayout.visibility = View.GONE
        }
        return true
    }
}
